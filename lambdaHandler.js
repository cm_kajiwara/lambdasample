var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var addWaterMark = require('./addWaterMark.js');
exports.handler = function(event,context){
    var bucket = event.Records[0].s3.bucket.name;
    var key = event.Records[0].s3.object.key;
    if(key.indexOf('-waterMark.png') > -1){
        context.done(null,'');
    }
    console.log('s3 get object');
    s3.getObject({Bucket:bucket,Key:key},function(err,data){
        if(err){
            console.log(err);
            context.done('error','error getting file' + err);
        }else{
            console.log('add water mark');
            addWaterMark.addWaterMark(data.Body,function(err,result){
                if(err){
                    console.log(err);
                    context.done('error','error add WaterMark' + err);
                }else{
                    console.log('put s3 object');
                    var waterMarkKey = key.split('.')[0] + '-waterMark.png';
                    s3.putObject({Bucket:bucket,Key:waterMarkKey,Body:new Buffer(result, 'binary')},function(err){
                        if(err){
                            console.log(err);
                            context.done('error','error s3 put object' + err);
                        }else{
                            context.done(null,'');
                        }
                    });
                }
            });
        }
    });
};
