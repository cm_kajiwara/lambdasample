'use strict';
var addWaterMark = require('../addWaterMark.js');
var targetFile = './testImage.jpg';
var dstFile = './resultFile.png';
var fs = require('fs');
var assert = require('assert');
before(function(done) {
    if (!fs.existsSync(targetFile)) {
        assert.fail();
    }
    if (fs.existsSync(dstFile)) {
        if (!fs.unlinkSync(dstFile)) {
            assert.fail();
        }
    }
    done();
});
describe('Add waterMark test', function() {
    it('Add waterMark test', function(done) {
        fs.readFile(targetFile, function(err, data) {
            if (!err) {
                addWaterMark.addWaterMark(data, function(err, result) {
                    if (!err) {
                        fs.writeFile(dstFile, result, 'binary', function(err) {
                            if (!err) {
                                done();
                            } else {
                                assert.fail();
                            }
                        });
                    } else {
                        console.log(err);
                        assert.fail();
                    }
                });
            } else {
                console.log(err);
                assert.fail();
            }
        });
    });
});
