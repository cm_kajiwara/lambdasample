'use strict';
var gm = require('gm');

exports.addWaterMark = function(createImageBuffer,callback){
    gm(createImageBuffer,'image.png').options({imageMagick: true}).composite('./waterMark.png').toBuffer('PNG',function(err,buffer){
        callback(err,buffer);
    });
};
